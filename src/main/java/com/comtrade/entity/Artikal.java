package com.comtrade.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class Artikal {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_artikla;
	private String naziv;
	private double cena;
	private String slika;
	                    
	@OneToMany(mappedBy = "porudzbina", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Stavke> setStavkiA = new HashSet<Stavke>();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_kategorije")
	private Kategorija kategorija;


	public Long getId_artikla() {
		return id_artikla;
	}


	public void setId_artikla(Long id_artikla) {
		this.id_artikla = id_artikla;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public double getCena() {
		return cena;
	}


	public void setCena(double cena) {
		this.cena = cena;
	}


	public String getSlika() {
		return slika;
	}


	public void setSlika(String slika) {
		this.slika = slika;
	}


	

	public Set<Stavke> getSetStavkiA() {
		return setStavkiA;
	}


	public void setSetStavkiA(Set<Stavke> setStavkiA) {
		this.setStavkiA = setStavkiA;
	}


	public Kategorija getKategorija() {
		return kategorija;
	}


	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}
	
	
	
}
