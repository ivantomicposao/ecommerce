package com.comtrade.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_usera;
	private String username;
	private String password;
	private int status;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Porudzbina> setPorudzbina = new HashSet<Porudzbina>();
	
	
	public void addPorudzbina(Porudzbina porudzbina) {
		setPorudzbina.add(porudzbina);
		porudzbina.setUser(this);
	}

	public void removePorudzbina(Porudzbina porudzbina) {
		setPorudzbina.remove(porudzbina);
		porudzbina.setUser(null);
	}
		
	
	public Long getId_usera() {
		return id_usera;
	}
	public void setId_usera(Long id_usera) {
		this.id_usera = id_usera;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Set<Porudzbina> getSetPorudzbina() {
		return setPorudzbina;
	}
	public void setSetPorudzbina(Set<Porudzbina> setPorudzbina) {
		this.setPorudzbina = setPorudzbina;
	}

	
}
